package de.fzi.ipe.ucm.test;

import static org.junit.Assert.assertTrue;

import java.util.Date;
import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Type;
import de.qsolutions.ucm.service.internal.FactQuery;
import de.qsolutions.ucm.service.internal.SchemaManager;
import de.qsolutions.ucm.service.provider.PullContextProvider;
import de.qsolutions.util.TimePeriod;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "/oracle-config.xml" })
public class QueryTestCase
{
	@Autowired protected PullContextProvider provider;
	@Autowired protected SchemaManager sm;
	
	protected final String featureURI = "http://www.fzi.de/ipe/ucm/test";

	@BeforeClass
	protected void setUp() throws Exception
	{
		BasicConfigurator.configure();		
		assertTrue(sm.addFeature(featureURI,Type.STRING,1,null,null,"exp",new double[] { 10,5 }));	
	}
	
	@AfterClass
	protected void tearDown()
	{
		sm.deleteFeature(featureURI);
		// update.clearFacts();
	}
	
	@Test
	public void testProvider()
	{
		final String featureURI = "http://www.fzi.de/ipe/ucm/test";
		
		FactQuery q = new FactQuery();
		q.setFeatureURIs(new String[] { featureURI });
		q.setValidPeriod(new TimePeriod(new Date(), null ));
		q.setMinimumCurrentConfidence(0.1);
		q.setUser("http://www.fzi.de/ipe/aschmidt");
			
		List<ContextFact> res = provider.getFacts(q);
		
		for (ContextFact fact : res)
		{
			System.out.println(fact.getValue());
		}
		
	}

}
