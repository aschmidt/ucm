package de.fzi.ipe.ucm.test;

import javax.sql.DataSource;

import org.apache.log4j.BasicConfigurator;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration( { "/oracle-config.xml" })
public class TestDataGenerator	
{
	@Autowired
	protected DataSource ds;
	
	@BeforeClass 
	public void setUp() throws Exception
	{
		BasicConfigurator.configure();
	}
		
	@Test
	public void createSchema() throws Exception
	{
	}
	
	public static void main(String[] args) throws Exception
	{
		TestDataGenerator t = new TestDataGenerator();
		t.setUp();
		t.createSchema();
	}
	
}
