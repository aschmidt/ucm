DROP TABLE datecontextfact;
DROP TABLE ontologycontextfact;
DROP TABLE numericalcontextfact;
DROP TABLE stringcontextfact;
DROP TABLE feature;
DROP TABLE featurehierarchy;
DROP TABLE datatypes;

DROP SEQUENCE factseq;
DROP SEQUENCE featureseq;
DROP TABLE users;


CREATE TABLE users
(
	userid VARCHAR2(255) PRIMARY KEY
);

CREATE SEQUENCE factseq;

CREATE SEQUENCE featureseq;

CREATE TABLE datatypes
(
  typeid VARCHAR2(255) PRIMARY KEY
);

INSERT INTO datatypes(typeid) VALUES('string');
INSERT INTO datatypes(typeid) VALUES('number');
INSERT INTO datatypes(typeid) VALUES('date');
INSERT INTO datatypes(typeid) VALUES('ontology');

CREATE TABLE feature
(
  featureid INTEGER PRIMARY KEY,
  uri VARCHAR2(255) NOT NULL,
  "type" VARCHAR2(255) NOT NULL,
  agingfunction VARCHAR(255),
  agingparam1 FLOAT,
  agingparam2 FLOAT,
  agingparam3 FLOAT,
  cardinality INTEGER NOT NULL
  domain VARCHAR2(255),
  range VARCHAR2(255)
);

ALTER TABLE feature ADD CONSTRAINT fk_feature_datatypes FOREIGN KEY ("type") REFERENCES datatypes(typeid);


CREATE UNIQUE INDEX featureURIIndex ON feature(uri);

CREATE TABLE featurehierarchy
(	
   parent INTEGER NOT NULL,
   child INTEGER NOT NULL   
);

ALTER TABLE featurehierarchy ADD CONSTRAINT pk_hierarchy PRIMARY KEY (parent,child);
ALTER TABLE featurehierarchy ADD CONSTRAINT fk_hierarchy1 FOREIGN KEY (parent) REFERENCES feature(featureid) ON DELETE CASCADE;
ALTER TABLE featurehierarchy ADD CONSTRAINT fk_hierarchy2 FOREIGN KEY (child)  REFERENCES feature(featureid) ON DELETE CASCADE;


-- Context Facts

CREATE TABLE numericalcontextfact
(
  id INTEGER PRIMARY KEY,
  featureid INTEGER NOT NULL,
  subject VARCHAR2(255) NOT NULL,
  subjectontology VARCHAR2(255) NOT NULL,
  validfrom TIMESTAMP,
  validuntil TIMESTAMP,
  confidence FLOAT NOT NULL,
  op VARCHAR2(10) NOT NULL,
  value NUMBER NOT NULL,
  transactiontime TIMESTAMP NOT NULL,
  transformedConfidence FLOAT NOT NULL
) ;

ALTER TABLE numericalcontextfact ADD CONSTRAINT fk_ncontextfact_feature FOREIGN KEY (featureid) REFERENCES feature(featureid) ON DELETE CASCADE;
ALTER TABLE numericalcontextfact ADD CONSTRAINT fk_ncfact_user FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE;

CREATE INDEX numconfidenceIndex ON numericalcontextfact(transformedConfidence);
CREATE INDEX numUserIndex ON numericalcontextfact(userid);
CREATE INDEX numFeatureIndex ON numericalcontextfact(featureid);
CREATE INDEX numValidIndex  ON numericalcontextfact(validFrom,validUntil);

CREATE TABLE ontologycontextfact
(
  id INTEGER PRIMARY KEY,
  featureid INTEGER NOT NULL,
  subject VARCHAR2(255) NOT NULL,
  subjectontology VARCHAR2(255) NOT NULL,
  validfrom TIMESTAMP,
  validuntil TIMESTAMP,
  confidence FLOAT NOT NULL,
  op VARCHAR2(10) NOT NULL,
  value VARCHAR2(255) NOT NULL,
  valueontology VARCHAR2(255) NOT NULL,
  source VARCHAR2(255) NOT NULL,
  transactiontime TIMESTAMP NOT NULL,
  transformedConfidence FLOAT NOT NULL
);

ALTER TABLE ontologycontextfact ADD CONSTRAINT fk_ontologycontextfact_feature FOREIGN KEY (featureid) REFERENCES feature (featureid) ON DELETE CASCADE;
ALTER TABLE ontologycontextfact ADD CONSTRAINT fk_ocfact_user FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE;

CREATE INDEX ontconfidenceIndex ON ontologycontextfact(transformedConfidence);
CREATE INDEX ontUserIndex ON ontologycontextfact(userid);
CREATE INDEX ontFeatureIndex ON ontologycontextfact(featureid);
CREATE INDEX ontValidIndex ON ontologycontextfact(validFrom,validUntil);

CREATE TABLE stringcontextfact
(
  id INTEGER PRIMARY KEY,
  featureid INTEGER NOT NULL,
  subject VARCHAR2(255) NOT NULL,
  subjectontology VARCHAR2(255) NOT NULL,
  validfrom TIMESTAMP,
  validuntil TIMESTAMP,
  confidence FLOAT NOT NULL,
  op VARCHAR2(10) NOT NULL,
  value VARCHAR2(255) NOT NULL,
  source VARCHAR2(255) NOT NULL,
  transactiontime TIMESTAMP NOT NULL,
  transformedConfidence FLOAT NOT NULL
) ;

ALTER TABLE stringcontextfact ADD CONSTRAINT fk_stringcontextfact_feature FOREIGN KEY (featureid) REFERENCES feature (featureid) ON DELETE CASCADE;
ALTER TABLE stringcontextfact ADD CONSTRAINT fk_scfact_user FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE;      

CREATE INDEX stringconfidenceIndex ON stringcontextfact(transformedConfidence);
CREATE INDEX stringconfidenceUserIndex ON stringcontextfact(userid);
CREATE INDEX stringFeatureIndex  ON stringcontextfact(featureid);
CREATE INDEX stringValidIndex  ON stringcontextfact(validFrom,validUntil);

CREATE TABLE datecontextfact
(
  id INTEGER PRIMARY KEY,
  featureid INTEGER NOT NULL,
  subject VARCHAR2(255) NOT NULL,
  subjectontology VARCHAR2(255) NOT NULL,
  validfrom TIMESTAMP,
  validuntil TIMESTAMP,
  confidence FLOAT NOT NULL,
  op VARCHAR2(10) NOT NULL,
  value TIMESTAMP NOT NULL,
  source VARCHAR2(255),
  transactiontime TIMESTAMP NOT NULL,
  transformedConfidence FLOAT NOT NULL
);

ALTER TABLE datecontextfact ADD CONSTRAINT fk_datecontextfact_feature FOREIGN KEY (featureid) REFERENCES feature(featureid) ON DELETE CASCADE;
ALTER TABLE datecontextfact ADD CONSTRAINT fk_dcfact_user FOREIGN KEY (userid) REFERENCES users(userid) ON DELETE CASCADE;      

CREATE INDEX dateconfidenceIndex ON datecontextfact(transformedConfidence);
CREATE INDEX dateconfidenceUserIndex ON datecontextfact(userid);
CREATE INDEX dateFeatureIndex  ON datecontextfact(featureid);
CREATE INDEX dateValidIndex  ON datecontextfact(validFrom,validUntil);

create or replace trigger feature_auto
  before insert on feature
  for each row
  begin
    select factseq.nextval into :new.featureid from dual;
  end;
/  

create or replace trigger ncontextfact_auto
  before insert on numericalcontextfact
  for each row
  begin
    select factseq.nextval into :new.id from dual;
  end;
/
    
create or replace trigger ocontextfact_auto
    before insert on ontologycontextfact
    for each row
    begin
      select factseq.nextval into :new.id from dual;
    end;
/
  
create or replace trigger stringcontextfact_auto
    before insert on stringcontextfact
    for each row
    begin
      select factseq.nextval into :new.id from dual;
    end;
/
  
create or replace trigger datecontextfact_auto
    before insert on datecontextfact
    for each row
    begin
      select factseq.nextval into :new.id from dual;
    end;
/
