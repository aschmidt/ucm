create or replace linearAging(currentDate as TIMESTAMP, 
							  initialDate as TIMESTAMP,
							  initialConfidence as DOUBLE, 
							  maxInterval as INTERVAL) 
							  
							  return DOUBLE
deterministic
as
	
	if (currentDate

	public double getConfidenceDecrease(Date initial, Date current,
			double initialConfidence)
	{
		long difference = current.getTime() - initial.getTime();
		
		if (difference > endDate)
			return 0;
		
		if (difference < 0)
			return 1;
		
		return ( 1 + (- 1 / endDate) * difference ); 
	}

	public Date getInitialDate(Date current, double confidenceDecrease)
	{
		if (confidenceDecrease == 1)
			return current;
		
		if (confidenceDecrease == 0)
		{
			return new Date((long) (current.getTime() - endDate));
		}
		
		return new Date((long) (current.getTime() - (endDate * (1 - confidenceDecrease))));
	}

	public void setParameters(double[] params)
	{
		this.endDate = params[0];
	}

	public Date getDropoutDate(Date initial, double confidenceDecrease)
	{
		
		return new Date(initial.getTime() + (long) ( endDate / confidenceDecrease ));
	}


end;
		