package de.qsolutions.ucm.model;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class OntologyEntity 
{
	public OntologyEntity(String uri, String ontologyURI)
	{
		this.uri = uri;
		this.ontologyURI = ontologyURI;
	}
	
	protected String uri;
	protected String ontologyURI;

	public String getOntologyURI()
	{
		return ontologyURI;
	}

	public void setOntologyURI(String ontologyURI)
	{
		this.ontologyURI = ontologyURI;
	}

	public String getURI()
	{
		return uri;
	}

	public void setURI(String uri)
	{
		this.uri = uri;
	}
	
	public boolean equals(Object o)
	{
		if (! (o instanceof OntologyEntity))
			return false;
	
		OntologyEntity e = (OntologyEntity) o;
		
		return e.getURI().equals(getURI());
	}
	
	public int hashCode()
	{
		return getURI().hashCode();
	}
	
	public String toString()
	{
		return getOntologyURI() + "#" + getURI();
	}
	
	public static OntologyEntity fromString(String s)
	{		
		int p = s.indexOf("#");
		
		if (p > -1)
			return new OntologyEntity(s.substring(p+1),s.substring(0, p));
		else
			return new OntologyEntity(s,"");
		
	}
}
