package de.qsolutions.ucm.model;

import java.util.Date;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface AgingFunction 
{
	public double getCurrentConfidence(Date initial, Date current, double initialConfidence);
	public Date getDropoutDate(Date initial, double initialConfidence, double minimumConfidence);
	
	public double getTransformedInitialConfidence(Date t0, double alpha0);
	public double getTransformedMinimumCondition(Date t, double alpha);

	public void setParameters(double[] params);
}
