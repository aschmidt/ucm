package de.qsolutions.ucm.model;

import java.io.Serializable;
import java.util.Date;

public enum Type implements Serializable
{
	NUMBER ("number", Double.class),
	STRING ("string", String.class),
	DATE ("date", Date.class),
	ONTOLOGY ("ontology", OntologyEntity.class);
	
	private String typeString;

	private Class<?> javaClass;
	
	private Type(String s, @SuppressWarnings("rawtypes") Class c)
	{
		typeString = s;
		javaClass = c;
	}
	
	public String toString()
	{
		return typeString;
	}
	
	public static Type fromObject(Object o)
	{
		if (o == null)
			return null;
		
		for (Type t : Type.values())
		{
			if (t.javaClass.isAssignableFrom(o.getClass()))
				return t;
		}
		return null;
	}
	
	public static Type fromString(String s)
	{
		for (Type t : Type.values())
		{
			if (t.toString().equals(s))
				return t;
		}
		return null;	
	}
}