package de.qsolutions.ucm.provider.relational;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

import de.qsolutions.ucm.model.AgingFunction;
import de.qsolutions.ucm.model.ContextFactImpl;
import de.qsolutions.ucm.model.FeatureInternal;
import de.qsolutions.ucm.model.Mode;
import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class RelationalContextFact extends ContextFactImpl
{
	public RelationalContextFact(FeatureInternal f)
	{
		this.feature = f;
	}

	public RelationalContextFact(FeatureInternal f, String userid,
			Date transactionTime, Date validFrom, Date validUntil, String op,
			String source, double confidence, Date current)
	{
		this(f);
		// this.subject = OntologyEntity.fromString(userid);
		this.subject = userid;
		this.transactionTime = transactionTime;
		this.validPeriod = new TimePeriod(validFrom, validUntil);
		this.operator = Mode.valueOf(op);
		this.source = source;
		this.confidence = confidence;

		computeCurrentConfidence(current);
	}

	public void fromResultSet(ResultSet rs, Date current) throws SQLException
	{
		// this.subject =
		// OntologyEntity.fromString(rs.getString(RelationalContextProvider.Fields.USER.fieldName));
		this.subject = rs
				.getString(RelationalContextProvider.Fields.SUBJECT.fieldName);
		this.transactionTime = rs
				.getTimestamp(RelationalContextProvider.Fields.TRANSACTION.fieldName);
		this.validPeriod = new TimePeriod(
				rs.getTimestamp(RelationalContextProvider.Fields.VALID_FROM.fieldName),
				rs.getTimestamp(RelationalContextProvider.Fields.VALID_UNTIL.fieldName));

		String op = rs
				.getString(RelationalContextProvider.Fields.MODE.fieldName);

		this.operator = Mode.valueOf(op);

		this.source = rs
				.getString(RelationalContextProvider.Fields.SOURCE.fieldName);
		this.confidence = rs
				.getDouble(RelationalContextProvider.Fields.INITIAL_CONFIDENCE.fieldName);

		computeCurrentConfidence(current);
	}

	protected void computeCurrentConfidence(Date current)
	{
		AgingFunction a = getFeature().getAgingFunction();

		if (a != null)
			this.currentConfidence = a.getCurrentConfidence(transactionTime,
					current, confidence);
		else
			this.currentConfidence = confidence;

	}

	public void setValue(Object value)
	{
		this.value = value;
	}
}