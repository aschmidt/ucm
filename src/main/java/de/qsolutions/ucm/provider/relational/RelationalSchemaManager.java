package de.qsolutions.ucm.provider.relational;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.Type;
import de.qsolutions.ucm.service.internal.AbstractSchemaManager;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class RelationalSchemaManager extends AbstractSchemaManager
{	
	protected Log log = LogFactory.getLog(getClass());
	
	protected DataSource dataSource;
	
	public void setDataSource(DataSource ds)
	{
		dataSource = ds;
	}
	
	public DataSource getDataSource()
	{
		return dataSource;
	}
	
	protected Connection getConnection() throws SQLException
	{
		return dataSource.getConnection();
	}

	protected Map<Long,RelationalFeatureInternal> features = new HashMap<Long,RelationalFeatureInternal>();
	
	protected String getParentFeatureQuery(long featureid)
	{
		return "SELECT feature.*,featurehierarchy.* " +
				"FROM featurehierarchy " +
				"INNER JOIN feature ON featurehierarchy.parent=feature.featureid " +
				"WHERE child=" + featureid;		
	}
	
	protected String getFeatureForURIQuery(String featureURI)
	{
		return "SELECT * FROM feature WHERE uri='" + featureURI + "'";
	}
	
	protected String getFeatureIDQuery(long id)
	{
		return "SELECT * FROM feature WHERE featureid=" + id;
	}
	
	protected String getRootFeatureQuery()
	{
		return "SELECT feature.*,featurehierarchy.* FROM featurehierarchy " +
		 	    "RIGHT OUTER JOIN feature ON featurehierarchy.child=feature.featureid " +
		 	    "WHERE parent IS NULL";
	}
	
	protected String getChildFeaturesQuery(long id)
	{
		return "SELECT feature.*,featurehierarchy.* FROM featurehierarchy "
				+ "INNER JOIN feature ON featurehierarchy.parent=feature.featureid WHERE parentid=" + id;
	}
		
	public List<Feature> getParentFeatures(Feature f)
	{
		long featureid = ((RelationalFeatureInternal) f).getFeatureID();
		String sql = getParentFeatureQuery(featureid);
		return getFeatureQueryResult(sql);
	}

	public Feature getFeatureForURI(String featureURI)
	{		
		return getSingleFeatureQueryResult(getFeatureForURIQuery(featureURI));
	}
	
	public RelationalFeatureInternal getFeatureForID(long id)
	{
		Long ID = new Long(id);

		if (features.containsKey(ID))
		{
			return features.get(ID);
		}
		else
		{			
			return getSingleFeatureQueryResult(getFeatureIDQuery(id));
		}

	}

	public boolean addFeature(String featureURI, Type type, int cardinality, List<String> parents, List<String> children, String agingFunction, double[] agingParams)
	{	
		String sql1 = "INSERT INTO feature(uri,\"type\", cardinality, agingfunction, agingParam1, agingParam2, agingParam3) " +
					  "VALUES('" + featureURI + "','" + type.toString() + "'," + cardinality + ",'" + agingFunction + "',";

		if (( agingParams == null) || (agingParams.length == 0))
			sql1 = sql1 + "NULL,NULL,NULL";
		else
		{
			switch (agingParams.length)
			{				
				case 1:
					sql1 = sql1 + agingParams[0] + ",NULL,NULL";
					break;
					
				case 2:
					sql1 = sql1 + agingParams[0] + "," + agingParams[1] + ",NULL";
					break;
				
				case 3:
					sql1 = sql1 + agingParams[0] + "," + agingParams[1] + "," + agingParams[2];
			}
		}
		
		sql1 = sql1 + ")";
		
		try 
		{
			Connection c = getConnection();
			Statement s = c.createStatement();
			s.executeUpdate(sql1);

			RelationalFeatureInternal rf = (RelationalFeatureInternal) getFeatureForURI(featureURI);		
			
			if (parents != null)
			{				
						
				String[] sql2 = new String[parents.size()];
				
				for (int i = 0; i < sql2.length; i++)
				{
					RelationalFeatureInternal pf = (RelationalFeatureInternal) getFeatureForURI(parents.get(i));
					sql2[i] = "INSERT INTO featurehierarchy(parent,child) VALUES(" + pf.getFeatureID() + "," + rf.getFeatureID() + ")";
				}
	
				for (String q : sql2)
				{
					try 
					{
						s.executeUpdate(q);
					}
					catch (SQLException ex)
					{
						log.error("",ex);
						return false;
					}
				}
			}
			
			if (children != null)
			{
				String[] sql3 = new String[children.size()];
				
				for (int i = 0; i < sql3.length; i++)
				{
					RelationalFeatureInternal cf = (RelationalFeatureInternal) getFeatureForURI(children.get(i));
					sql3[i] = "INSERT INTO featurehierarchy(parent,child) VALUES(" + rf.getFeatureID() + "," + cf.getFeatureID() + ")";
				}
	
				
				for (String q : sql3)
				{
					try 
					{
						s.executeUpdate(q);
					}
					catch (SQLException ex)
					{
						log.error("",ex);
						return false;
					}
				}
			}			
		} 
		catch (SQLException e) 
		{
			log.error(sql1,e);
			return false;
		}
		
		return true;
	}

	public boolean addFeature(String featureURI, Type type, int cardinality, List<String> parents, List<String> children, String agingFunction, double[] agingParams, String domain, String range)
	{
		if (! addFeature(featureURI, type, cardinality, parents, children, agingFunction, agingParams))
			return false;
		
		String sql1 = "UPDATE feature SET domain='" + domain + "' WHERE uri='" + featureURI + "'";
		String sql2 = "UPDATE feature SET range='" + range + "' WHERE uri='" + featureURI + "'";

		try 
		{
			Connection c = getConnection();
			Statement s = c.createStatement();
			s.executeUpdate(sql1);
			s.executeUpdate(sql2);
		} 
		catch (SQLException ex)
		{
			log.error("",ex);
			return false;

		}
		
		return true;
	}

	
	public boolean deleteFeature(String featureURI)
	{
		String sql = "DELETE FROM feature WHERE uri='" + featureURI + "'";
		
		try
		{
			Connection c = getConnection();
			Statement s = c.createStatement();
			s.executeUpdate(sql);
			return true;
		} 
		catch (SQLException e)
		{
			log.error(sql,e);
			return false;
		}
	}

	public List<Feature> getRootFeatures()
	{
		return getFeatureQueryResult(getRootFeatureQuery());
	}

	public List<Feature> getChildFeatures(Feature f)
	{
		RelationalFeatureInternal pf = (RelationalFeatureInternal) f;		
		return getFeatureQueryResult(getChildFeaturesQuery(pf.getFeatureID()));
	}

	protected List<Feature> getFeatureQueryResult(String sql)
	{
		List<Feature> res = new LinkedList<Feature>();
		
		Connection c = null;
		
		try
		{
			c = getConnection();
			Statement s = c.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			s.execute(sql);
			ResultSet rs = s.getResultSet();
			
			while (rs.next())
			{
				RelationalFeatureInternal p = new RelationalFeatureInternal(this, rs);
				features.put(new Long(p.getFeatureID()),p);
				
				res.add(p);
			}
		} 
		catch (SQLException e)
		{
			log.error("Error getting parent features: " + sql,e);
		}
		finally
		{
			try
			{
				c.close();
			} 
			catch (SQLException e)
			{
				log.error("",e);
			}			
		}
			
		return res;
		
	}
	
	protected RelationalFeatureInternal getSingleFeatureQueryResult(String sql)
	{
		Connection c = null;

		try
		{
			c =  getConnection();
			
			Statement s = c.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			s.execute(sql);
			ResultSet rs = s.getResultSet();
			rs.next();
			
			if (rs.isAfterLast())
			{
				log.error("Feature not found: " + sql);
				return null;
			}
			
			RelationalFeatureInternal p = new RelationalFeatureInternal(this, rs);
			features.put(new Long(p.getFeatureID()),p);

			try
			{
				c.close();
			} 
			catch (SQLException e)
			{
				log.error("",e);		
			}
			return p;
		}
		catch (SQLException ex)
		{
			log.error("Error resolving feature..." + sql, ex);

			try
			{
				c.close();
			} 
			catch (SQLException e)
			{
				log.error("",e);
			}
			return null;
		}
	}

	

}
