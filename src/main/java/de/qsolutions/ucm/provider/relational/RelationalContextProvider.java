package de.qsolutions.ucm.provider.relational;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.qsolutions.ucm.model.AgingFunction;
import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Type;
import de.qsolutions.ucm.query.QueryMode;
import de.qsolutions.ucm.service.internal.FactQuery;
import de.qsolutions.ucm.service.provider.PullContextProvider;
import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class RelationalContextProvider implements PullContextProvider
{
	private static Log log = LogFactory.getLog(RelationalContextProvider.class);
	
	protected static List<ContextFact> EMPTY_LIST = new LinkedList<ContextFact>();
	
	protected static Map<Type,String> tables = new HashMap<Type,String>();
		
	static
	{
		tables.put(Type.ONTOLOGY,"ontologycontextfact");
		tables.put(Type.DATE,"datecontextfact");
		tables.put(Type.STRING,"stringcontextfact");
		tables.put(Type.NUMBER,"numericalcontextfact");
	}
	
	public enum QuerySemantics
	{
		LOOSE,
		STRICT;
	}
	
	public enum Fields
	{
		ID("id"),
		FEATURE("featureid"),
		SUBJECT("subject"),
		VALID_FROM("validfrom"),
		VALID_UNTIL("validuntil"),
		INITIAL_CONFIDENCE("confidence"),
		MODE("op"),
		VALUE("value"),
		SOURCE("source"),
		TRANSACTION("transactiontime"),
		TRANFORMED_CONFIDENCE("transformedConfidence");

		private Fields(String s)
		{
			fieldName =s;
		}
		
		public String fieldName;
				
	};
	
	protected QuerySemantics validityComparisonSemantics = QuerySemantics.LOOSE;
	
	protected RelationalSchemaManager schemaManager;
	
	protected DataSource dataSource;
	
	protected RelationalConditionEvaluator conditionEvaluator = new RelationalConditionEvaluator();
	
	protected String buildQuery(FactQuery q)
	{
		//TODO currently assumes that all features have the same datatype
		RelationalFeatureInternal f = (RelationalFeatureInternal) schemaManager.getFeatureForURI(q.getFeatureURIs()[0]);
		String table = (String) tables.get(f.getType());
		
		StringBuffer sql = new StringBuffer();
		
		sql.append("SELECT * FROM " + table + " WHERE ");
		
		// deals with features
		for (int i = 0; i < q.getFeatureURIs().length; i++)
		{
			RelationalFeatureInternal fp = (RelationalFeatureInternal) schemaManager.getFeatureForURI(q.getFeatureURIs()[i]);
			sql.append(getFeatureQuery(fp,q));
			
			if (i != (q.getFeatureURIs().length -1))
				sql.append(" OR ");
		}
		
		// deals with valid time
		if (q.getValidPeriod() != null)
			sql.append(" AND " + getValidityIntervalQuery(q.getValidPeriod()));

		if (q.getValueCondition() != null)
			sql.append(" AND " + conditionEvaluator.getQueryCondition("subject",q.getValueCondition()));
		
		if (q.getMode() != QueryMode.BOTH)				
			sql.append(" AND " + vc(Fields.MODE, q.getMode().toString()));

		if (q.getValueCondition() != null)
			sql.append(" AND " + conditionEvaluator.getQueryCondition("value",q.getValueCondition()));
		
		return sql.toString();
	}
	
	protected String explode(long[] ids, String del)
	{
		StringBuffer b = new StringBuffer();
		
		for (int i = 0; i < ids.length - 1; i++)
			b.append(Long.toString(ids[i]) + ",");
		
		b.append(Long.toString(ids[ids.length - 1]));
		
		return b.toString();
	}
		
	public List<ContextFact> getFacts(FactQuery q)
	{		
		String sql = buildQuery(q);
		
		Connection c = null; 

		try
		{
			c = getConnection();
			Statement s = c.createStatement(
					ResultSet.TYPE_FORWARD_ONLY,
					ResultSet.CONCUR_READ_ONLY);
			
			s.execute(sql);
			ResultSet rs = s.getResultSet();			
			List<ContextFact> res = getFactsFromResultSet(rs,q.getValidPeriod().getBegin(),q.getMinimumCurrentConfidence());			
	
			c.close();			
			return res;
		}
		catch (SQLException ex)
		{
			log.error("Error executing query: \n " + sql.toString(), ex);
			
			try
			{
				c. close();
			}
			catch (SQLException e)  { }

			return EMPTY_LIST;
		}
	}

	
	public List<ContextFact> getFacts(long[] ids)
	{
		Connection c = null;
		List<ContextFact> res = new LinkedList<ContextFact>();
		String sql = null;
		
		for (String t : tables.values())
		{
			try
			{
				c = getConnection();
				Statement s = c.createStatement(
						ResultSet.TYPE_FORWARD_ONLY,
						ResultSet.CONCUR_READ_ONLY);
				
	
				sql = "SELECT * FROM " + t + " WHERE " + Fields.ID.fieldName + " IN (" + explode(ids,",") + ")";
				
				s.execute(sql.toString());	
				ResultSet rs = s.getResultSet();				
				res.addAll(getFactsFromResultSet(rs,new Date(),0));			
		
				c.close();
				
				return res;
			}
			catch (SQLException ex)
			{
				log.error("Error executing query: \n " + sql.toString(), ex);
				return EMPTY_LIST;
			}
			finally
			{
				try
				{
					c.close();
				}
				catch (SQLException e)  { }
			}
		}		
		
		return res;
	}
	
	protected List<ContextFact> getFactsFromResultSet(ResultSet rs, 
			Date reference, double minConfidence) throws SQLException
	{
		List<ContextFact> res = new LinkedList<ContextFact>();
		
		while (rs.next())
		{
			RelationalFeatureInternal fi = schemaManager.getFeatureForID(rs.getLong(Fields.FEATURE.fieldName));
			RelationalContextFact fact = fi.createContextFact(rs,reference);
			
			Date  maxValid = fi.getAgingFunction().getDropoutDate(
					fact.getTransactionTime(),fact.getInitialConfidence(),minConfidence);
			Date validUntil = new Date(Math.min(fact.getValidPeriod().getEnd().getTime(),maxValid.getTime()));
			
			fact.setValidPeriod(new TimePeriod(fact.getValidPeriod().getBegin(),validUntil));
			
			res.add(fact);			
		}
		
		return res;
	}
	
	protected String getFeatureQuery(RelationalFeatureInternal fp, FactQuery q)
	{
		StringBuffer sql = new StringBuffer();
		
		sql.append(" (" + vc(Fields.FEATURE, fp.getFeatureID()));
	
		AgingFunction a =  fp.getAgingFunction();		
		if (a != null)
		{
			double alpha = a.getTransformedMinimumCondition(q.getValidPeriod().getBegin(), q.getMinimumCurrentConfidence());		
			sql.append(" AND " + vc(Fields.INITIAL_CONFIDENCE,">= ", alpha));
		}
				
		sql.append(") ");

		return sql.toString();
	}

	public Set<String> getSupportedFeatures()
	{
		String sql = "SELECT * FROM feature";
		Set<String> res = new HashSet<String>(); 
		
		Connection c = null;
		try
		{
			c = getConnection();
			Statement s = c.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			s.execute(sql);
			ResultSet rs = s.getResultSet();
			
			while (rs.next())
				res.add(rs.getString("uri"));

			c.close();
				
		} 
		catch (SQLException e)
		{
			log.error("",e);
	
			try
			{
				c.close();
			}
			catch (SQLException ex) { }
		}
		
		return res;
	}
	
	public Set<String> getSupportedUsers()
	{
		String sql = "SELECT userid FROM users";
		Set<String> res = new HashSet<String>(); 
		
		Connection c = null;
		try
		{
			c = getConnection();
			Statement s = c.createStatement(ResultSet.TYPE_FORWARD_ONLY,ResultSet.CONCUR_READ_ONLY);
			s.execute(sql);
			ResultSet rs = s.getResultSet();
			
			while (rs.next())
				res.add(rs.getString("userid"));
			
			c.close();
		} 
		catch (SQLException e)
		{
			log.error("",e);

			try
			{
				c.close();
			}
			catch (SQLException ex) { }
		}
		
		return res;
	}
	
	protected String vc(Fields field, String comp, Object value)
	{
		if (value instanceof Date)
			return "(" + field.fieldName + comp + serialize((Date) value) + ")";

		
		return "(" + field.fieldName + comp + "'" + value.toString() + "')";
	}
	
	protected String vc(Fields field, Object value)
	{
		return vc(field,"=",value); 
	}
	
	protected String vc(Fields field, String comp, double v)
	{
		return "(" + field.fieldName + comp + Double.toString(v) + ")";
	}
	
	protected String vcN(Fields field, String comp, Object value)
	{
		return "((" + field.fieldName + " IS NULL) OR " + vc(field,comp,value) + ")";
	}
	
	protected String vcN(Fields field, Object value)
	{
		return vcN(field,"=",value);
	}
	
	public QuerySemantics getValidityComparisonSemantics()
	{
		return validityComparisonSemantics;
	}	
	
	private static final String FROM_NULL = "(" + Fields.VALID_FROM.fieldName + " IS NULL)";
	private static final String UNTIL_NULL = "(" + Fields.VALID_UNTIL.fieldName + " IS NULL)";
	
	protected String getValidityIntervalQuery(TimePeriod period)
	{
		Date from = period.getBegin();
		Date to = period.getEnd();
		
		StringBuffer sql = new StringBuffer();
		
		if (period.hasOpenStart())
		{
			if (! period.isOpenEnded())
			{
				switch (validityComparisonSemantics)
				{
					case STRICT:
						sql.append(vc(Fields.VALID_UNTIL, ">= " ,to));
						break;
					case LOOSE:
						sql.append(vcN(Fields.VALID_UNTIL, ">=", to));
						break;
				}
			}
		}
		else
		{
			if (period.isOpenEnded())
			{
				switch (validityComparisonSemantics)
				{
					case STRICT:
						sql.append("(" + vc(Fields.VALID_FROM, "<=" , from) + " AND " + vcN(Fields.VALID_UNTIL, ">= " , from) + ")");
						break;
					case LOOSE:
						sql.append("(" + vcN(Fields.VALID_FROM,"<=",from) + " AND " + vcN(Fields.VALID_UNTIL, ">= " , from) + ")");  
						break;
				}
			}
			else
			{
				switch (validityComparisonSemantics)
				{
					case STRICT:
						sql.append(getOverlapCondition(from,to));
						break;
					case LOOSE:
						sql.append("( ");
							sql.append("( " + FROM_NULL + " AND " + UNTIL_NULL + " )");
							sql.append(" OR ( " + FROM_NULL + " AND " + vc(Fields.VALID_UNTIL, ">=", to) + ") ");
							sql.append(" OR ( " + UNTIL_NULL + " AND " + vc(Fields.VALID_FROM, "<=" ,from) + ") ");							
							sql.append(" OR " + getOverlapCondition(from,to));
						sql.append(")");
						break;
				}
			}
		}
		
		return sql.toString();
	}

	protected boolean overlapSupported = true; 
	
	protected String getOverlapCondition(Date from, Date to)
	{
		if (overlapSupported)
			return "(( " + serialize(from) + "," + serialize(to) + ") OVERLAPS "
					+ "(" + Fields.VALID_FROM.fieldName + "," + Fields.VALID_UNTIL.fieldName + ") )";
		else
			return "(" + vc(Fields.VALID_FROM, "<=",from) + " AND "
				        + vc(Fields.VALID_UNTIL, ">=" ,from) + " ) OR "
				  + "(" + vc(Fields.VALID_FROM, ">=",from) + " AND "
				  		+ vc(Fields.VALID_FROM, "<=", to) + " )";
	}
	
	protected String serialize(Date d)
	{
		return serialize(new Timestamp(d.getTime()));
	}

	protected String serialize(Timestamp t)
	{
		return "(TIMESTAMP '" + t.toString() + "')";
	}

	/*
	 **********************************************************************************
	 * Getters and setters
	 *
	 */
	
	protected Connection getConnection() throws SQLException
	{
		return dataSource.getConnection();
	}
	
	public DataSource getDataSource()
	{
		return dataSource;
	}

	public void setDataSource(DataSource ds)
	{
		dataSource = ds;
	}

	public void setSchemaManager(RelationalSchemaManager schemaManager)
	{
		this.schemaManager = schemaManager;
	}

	public void setValidityComparisonSemantics(QuerySemantics s)
	{
		validityComparisonSemantics = s;
	}	

	public String getID()
	{
		return toString();
	}

	public RelationalSchemaManager getSchemaManager()
	{
		return schemaManager;
	}

	public RelationalConditionEvaluator getConditionEvaluator()
	{
		return conditionEvaluator;
	}

	public void setConditionEvaluator(
			RelationalConditionEvaluator conditionEvaluator)
	{
		this.conditionEvaluator = conditionEvaluator;
	}
	

}
