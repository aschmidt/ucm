package de.qsolutions.ucm.provider.relational.oracle;

import java.util.List;

import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.provider.relational.RelationalFeatureInternal;
import de.qsolutions.ucm.provider.relational.RelationalSchemaManager;

public class OracleSchemaManager extends RelationalSchemaManager 
{	
	public List<Feature> getAllParentFeatures(Feature f)
	{
		String hierarchyQuery = "SELECT h.parent " +
		"FROM featurehierarchy h " +
		"START WITH h.parent=" + ((RelationalFeatureInternal) f).getFeatureID() + " " +
		"CONNECT BY h.parent = PRIOR h.child";

		String sql = "SELECT feature.*,featurehierarchy.* " +
		"FROM featurehierarchy INNER JOIN feature ON featurehierarchy.parentid=feature.featureid " +
		"WHERE feature.featureid IN (" + hierarchyQuery + ")";
		
		return getFeatureQueryResult(sql);
	
	}

	public List<Feature> getAllChildFeatures(Feature f)
	{
		String hierarchyQuery = "SELECT h.parent " +
		"FROM featurehierarchy h " +
		"START WITH h.parent=" + ((RelationalFeatureInternal) f).getFeatureID() + " " +
		"CONNECT BY h.parent = PRIOR h.child";

		String sql = "SELECT feature.*,featurehierarchy.* " +
		"FROM featurehierarchy INNER JOIN feature ON featurehierarchy.parentid=feature.featureid " +
		"WHERE feature.featureid IN (" + hierarchyQuery + ")";
		
		return getFeatureQueryResult(sql);
	}
	
}
