package de.qsolutions.ucm.provider.relational;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import de.qsolutions.ucm.model.AgingFunction;
import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.OntologyEntity;
import de.qsolutions.ucm.service.internal.AbstractContextStorage;
import de.qsolutions.ucm.service.internal.AddFeatureValue;
import de.qsolutions.ucm.service.internal.SchemaManager;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class RelationalContextStorage extends AbstractContextStorage
{
	protected static Log log = LogFactory.getLog(RelationalContextStorage.class);

	protected SchemaManager schemaManager;

	protected Connection connection;

	protected DataSource dataSource;
	
	public void setDataSource(DataSource ds)
	{		
		dataSource = ds;
	
		try
		{
			if (connection != null)
				connection.close();
			
			connection = ds.getConnection();

			initialize();			
		}
		catch (SQLException ex)
		{
			log.error("Error initializing connection with prepared statements", ex);
		}			
	}
	
	protected Timestamp toTimestamp(Date d)
	{
		if (d == null)
			return null;
		else
			return new Timestamp(d.getTime());
	}
	
	protected String serialize(Date d)
	{
		return serialize(new Timestamp(d.getTime()));
	}
	
	protected String serialize(Timestamp t)
	{
		return "(TIMESTAMP '" + t.toString() + "')";
	}
	
	public SchemaManager getSchemaManager()
	{
		return schemaManager;
	}
	
	public void setSchemaManager(SchemaManager m)
	{
		schemaManager = m;
	}


	protected PreparedStatement insertOntology;
	protected PreparedStatement insertDate;
	protected PreparedStatement insertString;
	protected PreparedStatement insertNumber;

	protected void initialize() throws SQLException
	{
		insertOntology = connection.prepareStatement(
			    "INSERT INTO ontologycontextfact(featureid,userid,transactiontime,validfrom,validuntil,"
			  + "confidence,op,value,ontology,source, tranformedConfidence) "
			  + " VALUES (?,?,?,?,?,?,?,?,?,?,?)");
			
		insertDate = connection.prepareStatement(
				    "INSERT INTO datecontextfact(featureid,userid,transactiontime,validfrom,validuntil,"
				  + "confidence,op,value,source, transformedConfidence) "
				  + " VALUES (?,?,?,?,?,?,?,?,?,?)");

		insertString = connection.prepareStatement(
				    "INSERT INTO stringcontextfact(featureid,userid,transactiontime,validfrom,validuntil,"
				  + "confidence,op,value,source, transformedConfidence) "
				  + " VALUES (?,?,?,?,?,?,?,?,?,?)");

		insertNumber = connection.prepareStatement(
				    "INSERT INTO numericalcontextfact(featureid,userid,transactiontime,validfrom,validuntil,"
				  + "confidence,op,value,source, transformedConfidence) "
				  + " VALUES (?,?,?,?,?,?,?,?,?,?)");		
	}

	public boolean addFacts(List<AddFeatureValue> actions)
	{
		List<ContextFact> facts = new ArrayList<ContextFact>(actions.size());
		for (AddFeatureValue a : actions)
		{
			RelationalContextFact fact = null;
			try
			{
				RelationalFeatureInternal f = (RelationalFeatureInternal) schemaManager.getFeatureForURI(a.getFeatureURI());
				
				if (f == null)
				{
					log.error("Feature " + a.getFeatureURI() + " not found.");
					return false;
				}
				
				AgingFunction af = f.getAgingFunction();
				double transformedAlpha = af.getTransformedInitialConfidence(a.getTransactionTime(), a.getConfidence());
				
				fact = new RelationalContextFact(f,
						a.getUser(), a.getTransactionTime(),
						a.getValidFrom(), a.getValidUntil(),
						a.getOperator().toString(), a.getSource(),
						a.getConfidence(), new Date());
				
				switch (f.getType())
				{
					case ONTOLOGY:
						OntologyEntity e = (OntologyEntity) a.getValue();
						insertOntology(f.getFeatureID(),a.getUser(),a.getTransactionTime(),a.getValidFrom(),
								a.getValidUntil(),a.getConfidence(),a.getOperator().toString(),
								e.getURI(), e.getOntologyURI(), a.getSource(),transformedAlpha);
						fact.setValue(e);
						break;
						
					case NUMBER:
						insertNumber(f.getFeatureID(),a.getUser(),a.getTransactionTime(),a.getValidFrom(),
								a.getValidUntil(),a.getConfidence(),a.getOperator().toString(),
								(Number) a.getValue(), a.getSource(),transformedAlpha);
						fact.setValue(a.getValue());
						break;
						
					case STRING:
						insertString(f.getFeatureID(),a.getUser(),a.getTransactionTime(),a.getValidFrom(),
								a.getValidUntil(),a.getConfidence(),a.getOperator().toString(),
								(String) a.getValue(), a.getSource(),transformedAlpha);
						fact.setValue(a.getValue());
						break;
						
					case DATE:
						insertDate(f.getFeatureID(),a.getUser(),a.getTransactionTime(),a.getValidFrom(),
								a.getValidUntil(),a.getConfidence(),a.getOperator().toString(),
								(Date) a.getValue(), a.getSource(),transformedAlpha);
						
						fact.setValue(a.getValue());
				}
				
				facts.add(fact);
			}
			catch (SQLException ex)
			{
				log.error("Error adding context facts", ex);
				return false;				
			}
		}
		
		fireFactsAdded(facts);
		return true;
	}
	
	protected void insertOntology(long featureid, String userid, 
			Date transactionTime, Date validFrom, Date validUntil, double confidence,
			String op, String uri, String ontologyuri, String source, double transformedAlpha) throws SQLException
	{
		synchronized(insertOntology)
		{
			insertOntology.setLong(1,featureid);
			insertOntology.setString(2,userid);
			
			insertOntology.setTimestamp(3,toTimestamp(transactionTime));
			insertOntology.setTimestamp(4,toTimestamp(validFrom));
			insertOntology.setTimestamp(5,toTimestamp(validUntil));

			insertOntology.setDouble(6,confidence);
			
			insertOntology.setString(7,op);
			
			insertOntology.setString(8,uri);
			insertOntology.setString(9,ontologyuri);
			insertOntology.setString(10,source);
		
			insertOntology.setDouble(11, transformedAlpha);
			insertOntology.executeUpdate();			
		}
		
		
	}

	protected void insertDate(long featureid, String userid, 
			Date transactionTime, Date validFrom, Date validUntil, double confidence,
			String op, Date value, String source, double transformedAlpha) throws SQLException
	{
		synchronized(insertDate)
		{
			insertDate.setLong(1,featureid);
			insertDate.setString(2,userid);
			
			insertDate.setTimestamp(3,toTimestamp(transactionTime));
			insertDate.setTimestamp(4,toTimestamp(validFrom));
			insertDate.setTimestamp(5,toTimestamp(validUntil));

			insertDate.setDouble(6,confidence);

			insertDate.setString(7,op);

			insertDate.setTimestamp(8,toTimestamp(value));
			insertDate.setString(9,source);
			
			insertDate.setDouble(10, transformedAlpha);
			insertDate.executeUpdate();
		}		
	}

	protected void insertString(long featureid, String userid, 
			Date transactionTime, Date validFrom, Date validUntil, double confidence,
			String op, String value, String source, double transformedAlpha) throws SQLException
	{
		synchronized(insertString)
		{
			insertString.setLong(1,featureid);
			insertString.setString(2,userid);
			insertString.setTimestamp(3,toTimestamp(transactionTime));
			insertString.setTimestamp(4,toTimestamp(validFrom));
			insertString.setTimestamp(5,toTimestamp(validUntil));

			insertString.setDouble(6,confidence);

			insertString.setString(7,op);
			
			insertString.setString(8,value);			
			insertString.setString(9,source);
			
			insertString.setDouble(10, transformedAlpha);
			
			insertString.executeUpdate();			

		}
		
	}


	protected void insertNumber(long featureid, String userid, 
			Date transactionTime, Date validFrom, Date validUntil, double confidence,
			String op, Number n, String source, double transformedAlpha) throws SQLException
	{
		synchronized(insertNumber)
		{
			insertNumber.setLong(1,featureid);
			insertNumber.setString(2,userid);
			
			insertNumber.setTimestamp(3,toTimestamp(transactionTime));
			insertNumber.setTimestamp(4,toTimestamp(validFrom));
			insertNumber.setTimestamp(5,toTimestamp(validUntil));

			insertNumber.setDouble(6,confidence);

			insertNumber.setString(7,op);

			insertNumber.setDouble(8,n.doubleValue());
			insertNumber.setString(9,source);
			
			insertNumber.setDouble(10, transformedAlpha);
			
			insertNumber.executeUpdate();

		}
		
	}

	protected static final String[] DELETE_SQL = 
		{ 	"DELETE FROM ontologycontextfact",
			"DELETE FROM stringcontextfact",
			"DELETE FROM datecontextfact",
			"DELETE FROM numericalcontextfact"
		};
	
	public void clearFacts()
	{
		try
		{
			Statement s = connection.createStatement();
			for (String sql: DELETE_SQL)
				s.executeUpdate(sql);
		}
		catch (SQLException ex)
		{
			log.error("",ex);
		}
		
	}
		
}
