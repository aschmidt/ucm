package de.qsolutions.ucm.provider.ontology.kaon2;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.semanticweb.kaon2.api.DefaultOntologyResolver;
import org.semanticweb.kaon2.api.KAON2Connection;
import org.semanticweb.kaon2.api.KAON2Exception;
import org.semanticweb.kaon2.api.KAON2Manager;
import org.semanticweb.kaon2.api.Namespaces;
import org.semanticweb.kaon2.api.Ontology;
import org.semanticweb.kaon2.api.reasoner.Query;
import org.semanticweb.kaon2.api.reasoner.Reasoner;

import de.qsolutions.ucm.model.OntologyEntity;
import de.qsolutions.ucm.service.logical.OntologyManager;

public class KAON2OntologyManager implements OntologyManager
{

	public KAON2OntologyManager()
	{
		init();
	}

	protected Ontology ontology;
	protected KAON2Connection connection;
	
	public void setOntology(String uri)
	{
        try 
        {
			ontology=connection.openOntology(uri,new HashMap<String,Object>());
		} 
        catch (KAON2Exception e) 
        {
			e.printStackTrace();
		} catch (InterruptedException e) 
		{
			e.printStackTrace();
		}		

	}

	protected void init()
	{
        connection=KAON2Manager.newConnection();
        DefaultOntologyResolver resolver=new DefaultOntologyResolver();

        //TODO mappings
        //resolver.registerReplacement("http://www.fzi.de/ucm","file:ontology.xml");
        connection.setOntologyResolver(resolver);
	}
	
	
	
	public List<OntologyEntity> getInstances(OntologyEntity concept)
	{
		return getQueryResult("SELECT ?x WHERE { ?x rdf:type <" + concept.toString() + ">}");
	}

	public List<OntologyEntity> getQueryResult(String queryResult)
	{
		List<OntologyEntity> res = new LinkedList<OntologyEntity>();
        try 
        {
			
        	Reasoner reasoner=ontology.createReasoner();
			Query query=reasoner.createQuerySPARQL(Namespaces.INSTANCE,queryResult);

			query.open();
	        while (!query.afterLast()) 
	        {
	            res.add(OntologyEntity.fromString(query.tupleBuffer()[0].toString()));
	            query.next();
	        }
	        query.close();
	        query.dispose();
	        reasoner.dispose();

        } 
        catch (KAON2Exception e) 
        {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return res;
	}

}
