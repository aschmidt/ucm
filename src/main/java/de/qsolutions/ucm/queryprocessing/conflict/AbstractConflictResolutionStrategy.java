package de.qsolutions.ucm.queryprocessing.conflict;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.model.FeatureValue;
import de.qsolutions.ucm.model.FeatureValueImpl;
import de.qsolutions.ucm.model.Mode;
import de.qsolutions.ucm.service.internal.FactQuery;
import de.qsolutions.ucm.service.logical.ConflictResolutionStrategy;
import de.qsolutions.ucm.service.logical.FeatureValueQuery;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public abstract class AbstractConflictResolutionStrategy implements
		ConflictResolutionStrategy
{
	protected Comparator<ContextFact> comparator = null;
	
	public List<FeatureValue> filter(List<ContextFact> list, FeatureValueQuery q) 
	{
		double minConfidence = q.getConfidence();		
		List<FeatureValue> res = new LinkedList<FeatureValue>();
		
		for (Feature f : getTopmostFeatures(list))
			res.addAll(getValuesForFeature(list,f,minConfidence));
		
		return res;
	}

	protected Set<Feature> getTopmostFeatures(List<ContextFact> list)
	{
		Set<Feature> features = new HashSet<Feature>();
		Set<Feature> done = new HashSet<Feature>();
		
		
		for (ContextFact f : list)
		{
			Feature feature = f.getFeature();
			
			if (!done.contains(feature))
			{
				features.addAll(feature.getTopmostParents());				
				done.add(feature);
			}
		}
		
		return features;
	}

	public FactQuery[] getFactQueries(FeatureValueQuery q)
	{
		FactQuery res = new FactQuery();
		res.setMinimumCurrentConfidence(q.getConfidence());
		String[] s = new String[q.getFeatures().size()];
		res.setFeatureURIs(q.getFeatures().toArray(s));
		res.setMode(q.getMode());
		res.setSubjectCondition(q.getUser());
		res.setValidPeriod(q.getValidPeriod());
		return new FactQuery[] { res };
	}

	protected  Collection<FeatureValue> getValuesForFeature(
			List<ContextFact> facts, Feature f, double minConfidence)
	{
		facts = aggregateFacts(facts);
		facts = sortFacts(facts);
	
		Map<Object,FeatureValue> m = new HashMap<Object,FeatureValue>();
	
		int countPositive = 0;
	
		for(ContextFact fact : facts)
		{
			// is the value already in the result set (positive or negative)
			
			if (! m.containsKey(fact.getValue()) )
			{
				if (fact.getMode() == Mode.POSITIVE)
				{
					// is maximum cardinality already reached?
						
					if (countPositive < fact.getFeature().getCardinality())
					{
						m.put(fact.getValue(),createFeatureValue(fact));
						countPositive++;
					}
				}
				else
					m.put(fact.getValue(),createFeatureValue(fact));
			}
		}
		
		return m.values();
	}

	protected List<ContextFact> aggregateFacts(List<ContextFact> facts)
	{
		return facts;

	}
	protected List<ContextFact> sortFacts(List<ContextFact> facts)
	{
		Collections.sort(facts,comparator);
		return facts;
	}
	
	protected FeatureValue createFeatureValue(ContextFact rf)
	{		
		return new FeatureValueImpl(rf.getFeature(),rf.getSubject(),rf.getValidPeriod().getBegin(),rf.getValidPeriod().getEnd(),rf.getCurrentConfidence(),rf.getValue());
	}

}

