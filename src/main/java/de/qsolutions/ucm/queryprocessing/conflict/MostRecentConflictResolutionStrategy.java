package de.qsolutions.ucm.queryprocessing.conflict;

import java.util.Comparator;

import de.qsolutions.ucm.model.ContextFact;

public class MostRecentConflictResolutionStrategy extends
		AbstractConflictResolutionStrategy
{
	public MostRecentConflictResolutionStrategy()
	{
		super();
		comparator = new TransactionTimeComparator();
	}
	
	protected class TransactionTimeComparator implements Comparator<ContextFact>
	{

		public int compare(ContextFact arg0, ContextFact arg1)
		{
			return arg0.getTransactionTime().compareTo(arg1.getTransactionTime());
		}
		
	}

}
