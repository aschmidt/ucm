package de.qsolutions.ucm.queryprocessing.conflict;

import java.util.Comparator;

import de.qsolutions.ucm.model.ContextFact;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class MaximumConfidenceConflictResolutionStrategy extends AbstractConflictResolutionStrategy
{	

	public MaximumConfidenceConflictResolutionStrategy()
	{
		super();
		comparator = new ConfidenceComparator();
	}
	
	protected class ConfidenceComparator implements Comparator<ContextFact>
	{

		public int compare(ContextFact arg0, ContextFact arg1)
		{
			return Double.compare(arg0.getCurrentConfidence(),arg1.getCurrentConfidence());
		}
		
	}
}
