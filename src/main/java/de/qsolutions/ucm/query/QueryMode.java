package de.qsolutions.ucm.query;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum QueryMode
{
	POSITIVE("="),
	NEGATIVE("!="),
	BOTH("");
	
	private String op;
	
	private QueryMode(String s)
	{
		op = s;
	}
	
	public String toString()
	{
		return op;
	}
	
}
