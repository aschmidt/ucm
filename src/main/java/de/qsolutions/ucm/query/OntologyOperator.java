package de.qsolutions.ucm.query;

import de.qsolutions.ucm.model.OntologyEntity;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public enum OntologyOperator implements Operator<OntologyEntity,String[]>
{
	EQUALS("=",1),
	INSTANCE_OF("instanceOf",1),
	RELATED_TO("relateToConceptInstance",2),
	IN("IN",1);
	
	private String op;
	private int argCount;
	
	private OntologyOperator(String op, int i)
	{
		this.op = op;
		argCount = i;
	}
	
	public String operator()
	{
		return op;
	}
	
	public int argCount()
	{
		return argCount;
	}

	public boolean evaluate(OntologyEntity value, AtomicCondition<OntologyEntity,String[]> c)
	{
		switch (this)
		{
			case EQUALS:
				return OntologyEntity.fromString(c.getArg()[0]).equals(value);

			case IN:
				for (String s : c.getArg())
				{
					if (OntologyEntity.fromString(s).equals(value))
						return true;
				}
				return false;

			case INSTANCE_OF:
			case RELATED_TO:
				throw new IllegalArgumentException("Ontology reasoning not implemented in operator.");

			// TODO
		}
		return false;
	}
	
}
