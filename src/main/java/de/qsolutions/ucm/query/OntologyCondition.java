package de.qsolutions.ucm.query;

import de.qsolutions.ucm.model.OntologyEntity;


/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class OntologyCondition extends AtomicCondition<OntologyEntity,String[]> 
{	
	public OntologyCondition(OntologyOperator op, String entity)
	{
		super(op,new String[] { entity } );
	}

	public OntologyCondition(OntologyOperator op, String[] s2)
	{
		super(op,s2);
	}
}
