package de.qsolutions.ucm.query;

import java.util.Iterator;
import java.util.List;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class CompositeCondition implements Condition, Iterable<Condition>
{
	protected Connector connector;
	protected List<Condition> childConditions;

	public void addCondition(Condition c)
	{
		childConditions.add(c);
	}
	
	public Connector getConnector()
	{
		return connector;
	}

	public Iterator<Condition> iterator()
	{
		return childConditions.iterator();
	}
}
