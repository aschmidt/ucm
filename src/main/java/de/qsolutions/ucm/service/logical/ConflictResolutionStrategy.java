package de.qsolutions.ucm.service.logical;

import java.util.List;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.FeatureValue;
import de.qsolutions.ucm.service.internal.FactQuery;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ConflictResolutionStrategy
{
	List<FeatureValue> filter(List<ContextFact> list, FeatureValueQuery q);
	FactQuery[]		   getFactQueries(FeatureValueQuery q);
}
