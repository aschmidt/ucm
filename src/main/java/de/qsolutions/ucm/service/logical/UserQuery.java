package de.qsolutions.ucm.service.logical;

import de.qsolutions.ucm.query.Condition;
import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class UserQuery
{
	public static final String ALL_USERS = "##ALL##";
	
	protected double confidence = 0;	
	protected Condition condition;
	protected TimePeriod validPeriod;

	/**
	 * @return Returns the condition.
	 */
	public Condition getCondition()
	{
		return condition;
	}

	/**
	 * @param condition The condition to set.
	 */
	public void setCondition(Condition condition)
	{
		this.condition = condition;
	}

	/**
	 * @return Returns the confidence.
	 */
	public double getConfidence()
	{
		return confidence;
	}

	/**
	 * @param confidence The confidence to set.
	 */
	public void setConfidence(double confidence)
	{
		this.confidence = confidence;
	}

	public TimePeriod getValidPeriod()
	{
		return validPeriod;
	}

	public void setValidPeriod(TimePeriod validPeriod)
	{
		this.validPeriod = validPeriod;
	}
}
