package de.qsolutions.ucm.service.logical;

import java.util.List;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.query.Condition;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface QueryEvaluator
{
	public TruthValue evaluateCondition(Condition c, List<ContextFact> facts);	
	
	public enum TruthValue
	{
		TRUE,
		FALSE,
		UNKNOWN,
		CONTRADICTORY
	}
}
