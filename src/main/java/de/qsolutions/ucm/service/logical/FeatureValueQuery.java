package de.qsolutions.ucm.service.logical;

import java.util.Set;

import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.QueryMode;
import de.qsolutions.util.TimePeriod;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class FeatureValueQuery
{
	public static final String ALL_USERS = "##ALL##";
	
	protected String user = ALL_USERS;
	protected Set<String> features = null;
	protected double confidence = 0;
	protected QueryMode mode = QueryMode.BOTH;
	protected TimePeriod validPeriod;
	
	protected Condition valueCondition;

	/**
	 * @return Returns the condition.
	 */
	public Condition getValueCondition()
	{
		return valueCondition;
	}

	/**
	 * @param condition The condition to set.
	 */
	public void setValueCondition(Condition valueCondition)
	{
		this.valueCondition = valueCondition;
	}

	/**
	 * @return Returns the confidence.
	 */
	public double getConfidence()
	{
		return confidence;
	}

	/**
	 * @param confidence The confidence to set.
	 */
	public void setConfidence(double confidence)
	{
		this.confidence = confidence;
	}

	/**
	 * @return Returns the features.
	 */
	public Set<String> getFeatures()
	{
		return features;
	}

	/**
	 * @param features The features to set.
	 */
	public void setFeatures(Set<String> features)
	{
		this.features = features;
	}

	/**
	 * @return Returns the mode.
	 */
	public QueryMode getMode()
	{
		return mode;
	}

	/**
	 * @param mode The mode to set.
	 */
	public void setMode(QueryMode mode)
	{
		this.mode = mode;
	}

	/**
	 * @return Returns the user.
	 */
	public String getUser()
	{
		return user;
	}

	/**
	 * @param user The user to set.
	 */
	public void setUser(String user)
	{
		this.user = user;
	}

	public TimePeriod getValidPeriod()
	{
		return validPeriod;
	}

	public void setValidPeriod(TimePeriod validPeriod)
	{
		this.validPeriod = validPeriod;
	}
}
