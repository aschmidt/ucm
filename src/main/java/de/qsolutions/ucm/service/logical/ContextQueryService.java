package de.qsolutions.ucm.service.logical;

import java.util.List;

import de.qsolutions.ucm.model.FeatureValue;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextQueryService 
{
	/**
	 * This method returns a list of feature values compliant to the query
	 * and compliant to the underlying schema, i.e., conflicts are resolved.
	 * The individual values are annotated with the confidence values for
	 * the considered time period.
	 *
	 * @param q
	 * @return
	 */
	public List<FeatureValue> 	getValuesForUser(FeatureValueQuery q);	
	public List<String> 		getUsers(UserQuery q);
}
