package de.qsolutions.ucm.service.provider;

import de.qsolutions.ucm.service.internal.ContextFactSubscriber;
import de.qsolutions.ucm.service.internal.FactQuery;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface PushContextProvider extends ContextProvider
{
	public String subscribe(FactQuery q, ContextFactSubscriber subscriber);
	public void  unsubscribe(String id);
}
