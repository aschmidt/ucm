package de.qsolutions.ucm.service.internal;

import java.util.Date;

import de.qsolutions.ucm.model.ContextFact;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class AnnotatedContextFact
{
	protected ContextFact fact;
	protected Date dropOutDate;
	protected NotificationType type;
	
	public static enum NotificationType
	{
		ADD,
		REVOKE
	}
	
	public AnnotatedContextFact(ContextFact f)
	{
		this.fact = f;
	}

	public ContextFact getFact()
	{
		return fact;
	}

	public void setFact(ContextFact fact)
	{
		this.fact = fact;
	}

	public Date getDropOutDate()
	{
		return dropOutDate;
	}

	public void setDropOutDate(Date dropOutDate)
	{
		this.dropOutDate = dropOutDate;
	}

	public NotificationType getType()
	{
		return type;
	}

	public void setType(NotificationType type)
	{
		this.type = type;
	}

}
