package de.qsolutions.ucm.service.internal;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;

import de.qsolutions.ucm.model.Feature;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public abstract class AbstractSchemaManager implements SchemaManager
{
	public List<Feature> getAllChildFeatures(Feature f)
	{
		Set<Feature> res = new HashSet<Feature>();
		Queue<Feature> todo = new LinkedList<Feature>();
		todo.add(f);
		
		Feature c = null;
		
		while ( (c = todo.poll()) != null)
		{
			List<Feature> l = getChildFeatures(c);
			
			for (Feature ff : l)
			{
				if (! res.contains(ff))
					todo.add(ff);
			}

			res.addAll(l);
		}
		
		
		return new LinkedList<Feature>(res);
	}

	public List<Feature> getAllParentFeatures(Feature f)
	{
		Set<Feature> res = new HashSet<Feature>();
		Queue<Feature> todo = new LinkedList<Feature>();
		todo.add(f);
		
		Feature c = null;
		
		while ( (c = todo.poll()) != null)
		{
			List<Feature> l = getParentFeatures(c);
			
			for (Feature ff : l)
			{
				if (! res.contains(ff))
					todo.add(ff);
			}

			res.addAll(l);
		}
		
		
		return new LinkedList<Feature>(res);
	}

}
