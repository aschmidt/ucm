package de.qsolutions.ucm.service.internal;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.service.internal.impl.DefaultContextFactService;
import de.qsolutions.ucm.service.provider.PullContextProvider;
import de.qsolutions.ucm.service.provider.PushContextProvider;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class ContextServiceContextProviderWrapper extends DefaultContextFactService implements PullContextProvider, PushContextProvider
{
	public String getID()
	{
		return Long.toString(hashCode());
	}
	
	public Set<String> getSupportedFeatures()
	{
		Set<String> res = new HashSet<String>();
		res.addAll(providerRegistry.getSupportedFeatures());
		return res;
	}

	public Set<String> getSupportedUsers()
	{
		Set<String> res = new HashSet<String>();
		res.addAll(providerRegistry.getSupportedUsers());
		return res;
	}
	
	public String subscribe(FactQuery q, ContextFactSubscriber subscriber)
	{
		q.setFeatureURIs(rewriteFeatureURIs(q.featureURIs));
		return publisher.subscribe(subscriber,q);
	}

	public void unsubscribe(String id)
	{
		publisher.unsubscribe(id);
	}

	protected String[] rewriteFeatureURIs(String[] features)
	{
		Set<String> f = new HashSet<String>();
		f.addAll(Arrays.asList(features));
		
		for (String s : features)
		{
			List<Feature> l = schemaManager.getAllChildFeatures(schemaManager.getFeatureForURI(s));
			
			for (Feature ff : l)
				f.add(ff.getURI());
		}
		
		String[] res = new String[f.size()];
		f.toArray(res);
		return res;
	}
}
