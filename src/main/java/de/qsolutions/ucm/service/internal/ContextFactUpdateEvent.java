package de.qsolutions.ucm.service.internal;

import java.util.List;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class ContextFactUpdateEvent
{
	protected List<AnnotatedContextFact> facts;

	public ContextFactUpdateEvent(List<AnnotatedContextFact> facts)
	{
		this.facts = facts;
	}
	
	public List<AnnotatedContextFact> getFacts()
	{
		return facts;
	}	
}
