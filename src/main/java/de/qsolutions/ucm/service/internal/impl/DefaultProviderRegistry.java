package de.qsolutions.ucm.service.internal.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.qsolutions.ucm.service.internal.ContextProviderRegistry;
import de.qsolutions.ucm.service.provider.ContextProvider;
import de.qsolutions.util.MultiValueMap;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

@Component
public class DefaultProviderRegistry implements ContextProviderRegistry
{
	protected MultiValueMap<String,ContextProvider> subjects = new MultiValueMap<String,ContextProvider>();
	protected MultiValueMap<String,ContextProvider> features = new MultiValueMap<String,ContextProvider>();
	
	protected HashMap<String,ContextProvider> providers = new HashMap<String,ContextProvider>();
	
	public void setProviders(List<ContextProvider> pl)
	{
		for (ContextProvider p : pl)
			register(p);
	}
	
	public void register(ContextProvider provider)
	{
		for (String s : provider.getSupportedSubjects())
			subjects.put(s,provider);

		for (String s : provider.getSupportedFeatures())
			features.put(s,provider);

		providers.put(provider.getID(),provider);
	}

	public List<ContextProvider> getProviders(String featureURI, String userID)
	{
		if (featureURI == null)
			return getProviders(new String[0], userID);
		else
			return getProviders(new String[] { featureURI }, userID);
	}

	public void unregister(String id)
	{
		ContextProvider p = providers.get(id);
		
		if (p != null)
		{
			for (String s : p.getSupportedSubjects())
				subjects.remove(s,p);
			
			for (String f : p.getSupportedFeatures())
				features.remove(f,p);
		}
	}

	public List<ContextProvider> getProviders(String[] featureURIs, String userID)
	{
		List<ContextProvider> res = new LinkedList<ContextProvider>();

		if (featureURIs.length == 0)
		{
			if (userID != null)
			{
				res.addAll(subjects.get(userID));
				return res;
			}
			else
			{
				return res;
			}
		}
		else
		{
			if (userID == null)
			{
				for (String featureURI : featureURIs)
					res.addAll(features.get(featureURI));
				
				return res;
			}
			else
			{
				for (String featureURI : featureURIs)
				{
					List<ContextProvider> l1 = subjects.get(userID);
					List<ContextProvider> l2 = features.get(featureURI);
					
					l1.retainAll(l2);
					res.addAll(l1);
				}
				
				return res;
			}
		}
	}

	public List<String> getSupportedUsers()
	{
		List<String> res = new LinkedList<String>();		
		res.addAll(subjects.keySet());
		return res;
	}

	public List<String> getSupportedFeatures()
	{
		List<String> res = new LinkedList<String>();
		res.addAll(features.keySet());
		return res;
	}
}
