package de.qsolutions.ucm.service.internal;

import java.util.Date;

import de.qsolutions.ucm.model.Mode;
import de.qsolutions.ucm.model.Type;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public class AddFeatureValue
{
	protected String featureURI;
	protected String user;
	protected Date validFrom;
	protected Date validUntil;
	protected double confidence;
	protected Object value;
	protected Date transactionTime;
	protected String source;
	protected Mode operator;
	protected Type type;
	
	public double getConfidence()
	{
		return confidence;
	}
	public void setConfidence(double confidence)
	{
		this.confidence = confidence;
	}
	public String getFeatureURI()
	{
		return featureURI;
	}
	public void setFeatureURI(String featureURI)
	{
		this.featureURI = featureURI;
	}
	public Mode getOperator()
	{
		return operator;
	}
	public void setOperator(Mode operator)
	{
		this.operator = operator;
	}
	public String getSource()
	{
		return source;
	}
	public void setSource(String source)
	{
		this.source = source;
	}
	public Date getTransactionTime()
	{
		return transactionTime;
	}
	public void setTransactionTime(Date transactionTime)
	{
		this.transactionTime = transactionTime;
	}
	public String getUser()
	{
		return user;
	}
	public void setUser(String user)
	{
		this.user = user;
	}
	public Date getValidFrom()
	{
		return validFrom;
	}
	public void setValidFrom(Date validFrom)
	{
		this.validFrom = validFrom;
	}
	public Date getValidUntil()
	{
		return validUntil;
	}
	public void setValidUntil(Date validUntil)
	{
		this.validUntil = validUntil;
	}
	public Object getValue()
	{
		return value;
	}
	public void setValue(Object value)
	{
		this.value = value;
	
		type = Type.fromObject(value);
	}
	
	public Type getType()
	{
		return type;
	}
}
