package de.qsolutions.ucm.service.internal.impl;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.springframework.stereotype.Component;

import de.qsolutions.ucm.model.ContextFact;
import de.qsolutions.ucm.model.Feature;
import de.qsolutions.ucm.service.internal.AddFeatureValue;
import de.qsolutions.ucm.service.internal.ContextFactPublisher;
import de.qsolutions.ucm.service.internal.ContextFactService;
import de.qsolutions.ucm.service.internal.ContextProviderRegistry;
import de.qsolutions.ucm.service.internal.ContextStorage;
import de.qsolutions.ucm.service.internal.FactQuery;
import de.qsolutions.ucm.service.internal.SchemaManager;
import de.qsolutions.ucm.service.provider.ContextProvider;
import de.qsolutions.ucm.service.provider.PullContextProvider;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

@Component
public class DefaultContextFactService implements ContextFactService
{
	protected int timeout= 1000 * 60;
	
	protected ContextProviderRegistry providerRegistry;
	protected SchemaManager schemaManager;
	protected ContextStorage storage;
	protected ContextFactPublisher publisher;
	
	/* (non-Javadoc)
	 * @see de.fzi.ipe.ucm.service.internal.ContextFactService#getFacts(de.fzi.ipe.ucm.service.internal.FactQuery)
	 */
	public List<ContextFact> getFacts(FactQuery q)
	{
		if (q.getFeatureURIs() != FactQuery.ALL_FEATURES)
			q.setFeatureURIs(getSubsumedFeatures(q.getFeatureURIs()));

		//TODO
		List<ContextProvider> providers = providerRegistry.getProviders(
				q.getFeatureURIs(), q.getUser());
		List<ContextFact> res = new LinkedList<ContextFact>();
		
		CountDownLatch l = new CountDownLatch(providers.size());
		
		for (ContextProvider p : providers)
		{
			if (p instanceof PullContextProvider)
				new Thread(new ProviderWorker(l,(PullContextProvider) p,q,res)).start();
		}
		
		try
		{
			l.await(timeout,TimeUnit.MILLISECONDS);
		}
		catch (InterruptedException ex)
		{
		}
		
		return res;	
	}
		
	protected String[] getSubsumedFeatures(String[] fs)
	{		
		Set<String> res = new HashSet<String>();
		
		for (String featureURI : fs)
		{
			Feature f = schemaManager.getFeatureForURI(featureURI);
			List<Feature> features = schemaManager.getChildFeatures(f);
			res.add(featureURI);
			
			for (Feature f_ : features)
				res.add(f_.getURI());
		}
		
		String[] s = new String[res.size()];
		res.toArray(s);
		return s; 
	}
	
	
	public void update(List<AddFeatureValue> updates)
	{
		storage.addFacts(updates);
	}
	
	protected static class ProviderWorker implements Runnable
	{
		protected List<ContextFact> resultList;
		protected PullContextProvider provider;
		protected FactQuery query;
		protected CountDownLatch signal;
			
		public ProviderWorker(CountDownLatch l, PullContextProvider p, FactQuery pq, List<ContextFact> res)
		{
			this.provider = p;
			this.resultList = res;
			this.query = pq;
			this.signal = l;
		}
		public void run()
		{
			List<ContextFact> r = provider.getFacts(query);
			
			synchronized(resultList)
			{
				resultList.addAll(r);
			}
			
			signal.countDown();
		}		
	}

	public ContextProviderRegistry getProviderRegistry()
	{
		return providerRegistry;
	}

	public void setProviderRegistry(ContextProviderRegistry providerRegistry)
	{
		this.providerRegistry = providerRegistry;
	}

	public SchemaManager getSchemaManager()
	{
		return schemaManager;
	}

	public void setSchemaManager(SchemaManager schemaManager)
	{
		this.schemaManager = schemaManager;
	}

	public ContextStorage getStorage()
	{
		return storage;
	}

	public void setStorage(ContextStorage storage)
	{
		this.storage = storage;
	}

	public int getTimeout()
	{
		return timeout;
	}

	public void setTimeout(int timeout)
	{
		this.timeout = timeout;
	}

	public ContextFactPublisher getPublisher()
	{
		return publisher;
	}

	public void setPublisher(ContextFactPublisher publisher)
	{
		this.publisher = publisher;
	}
}
