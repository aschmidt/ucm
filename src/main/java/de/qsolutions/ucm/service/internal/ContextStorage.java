package de.qsolutions.ucm.service.internal;

import java.util.List;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextStorage 
{
	public boolean addFacts(List<AddFeatureValue> actions);
	
	public void clearFacts();
	
	public void addStorageListener(ContextStorageListener l);
	public void removeStorageListener(ContextStorageListener l);
}
