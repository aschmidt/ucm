package de.qsolutions.ucm.service.internal;

/**
 * @author Andreas Schmidt <aschmidt@fzi.de>
 */

public interface ContextFactPublisher
{
	public String subscribe(ContextFactSubscriber s, FactQuery q);
	public void unsubscribe(String subscriptionID);
}
