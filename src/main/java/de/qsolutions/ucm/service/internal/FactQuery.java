package de.qsolutions.ucm.service.internal;

import java.util.Date;

import de.qsolutions.ucm.query.Condition;
import de.qsolutions.ucm.query.QueryMode;
import de.qsolutions.util.TimePeriod;
import de.qsolutions.ucm.query.TrueCondition;

/**
 * The semantics of the query is as follows:
 * 
 * A context fact matches if 
 * - its feature is the same or a subfeature of the given feature uri
 * - the interval intersects with the validity interval (null means unconstrained)
 * - the confidence is equal or more than the specified minimum confidence
 * 
 * @author Andreas Schmidt (aschmidt@fzi.de)
 */

public class FactQuery
{
	public static final TrueCondition TRUE_CONDITION = new TrueCondition();

	public static final String ALL_SOURCES = "##ALL##";
	public static final String[] ALL_FEATURES = { "##ALL##" };
	
	protected String[] featureURIs = ALL_FEATURES;
	protected double minimumCurrentConfidence = 0;
	protected double minimumInitialConfidence = 0;
	protected QueryMode mode = QueryMode.BOTH;
	protected String source = ALL_SOURCES;

	protected Condition subjectCondition;
	// TODO valueCondition (nur boolesche Kombination von direkt ausführbaren Bedingungen)
	protected Condition valueCondition;
	
	protected TimePeriod valid;
	protected TimePeriod transaction;

	protected static final long DATE_INTERVALL_SIZE = 1;
	
	public FactQuery()
	{
		Date now = new Date();
		valid = new TimePeriod(new Date[] {now, new Date(now.getTime()+DATE_INTERVALL_SIZE)});
		transaction = new TimePeriod(new Date[] { null, now });		
	}
		
	public String[] getFeatureURIs()
	{
		return featureURIs;
	}

	public void setFeatureURIs(String[] featureURIs)
	{
		this.featureURIs = featureURIs;
	}

	public TimePeriod getValidPeriod()
	{
		return valid;
	}

	public void setValidPeriod(TimePeriod interval)
	{
		this.valid = interval;
	}

	public double getMinimumCurrentConfidence()
	{
		return minimumCurrentConfidence;
	}

	public QueryMode getMode()
	{
		return mode;
	}

	public void setMode(QueryMode mode)
	{
		this.mode = mode;
	}

	public String getSource()
	{
		return source;
	}

	public void setSource(String source)
	{
		this.source = source;
	}

	public TimePeriod getTransactionPeriod()
	{
		return transaction;
	}

	public void setTransactionPeriod(TimePeriod transaction)
	{
		this.transaction = transaction;
	}

	public void setMinimumCurrentConfidence(double minimumCurrentConfidence)
	{
		this.minimumCurrentConfidence = minimumCurrentConfidence;
	}

	public double getMinimumInitialConfidence()
	{
		return minimumInitialConfidence;
	}

	public void setMinimumInitialConfidence(double minimumInitialConfidence)
	{
		this.minimumInitialConfidence = minimumInitialConfidence;
	}

	public Condition getValueCondition()
	{
		return valueCondition;
	}

	public void setValueCondition(Condition valueCondition)
	{
		this.valueCondition = valueCondition;
	}

	public Condition getSubjectCondition()
	{
		return subjectCondition;
	}

	public void setSubjectCondition(Condition subjectCondition)
	{
		this.subjectCondition = subjectCondition;
	}	
}
